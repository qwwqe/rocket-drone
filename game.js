// Rocket Drone
// Copyright (C) 2022 wavy (wavy@optime.ca)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Drone {
	constructor(x, y){
		this.x = x
		this.y = y
		this.vx = 0
		this.vy = 0
		this.a = 0
		this.va = 0
		
		this.fuel = 100
		this.efficiency = 20
		this.damage = 0
		this.broken = false
		
		this.I = 20		// moment of inertia
		this.m = 1		// mass
		this.R = 1		// moment arm length
		this.g = 1		// gravity
		this.b = 0.4	// rotational drag coefficient
		this.k = 0.4	// wall friction coefficient
		
		this.damage_threshold = 12	// contact force must be greater than this to accumulate damage
		
		this.hbw = 80	// hit box width
		this.hbh = 20	// hit box height
		this.hbr = 10	// hit box radius

		this.kinetic_energy = 0
	}
	
	get xl(){
		return this.x - Math.cos(this.a)*this.hbw/2
	}
	
	get xr(){
		return this.x + Math.cos(this.a)*this.hbw/2
	}
	
	get yl(){
		return this.y + Math.sin(this.a)*this.hbw/2
	}
	
	get yr(){
		return this.y - Math.sin(this.a)*this.hbw/2
	}
	
	get vxl(){
		return this.vx + Math.sin(this.a)*this.va*this.hbw/2
	}
	
	get vxr(){
		return this.vx - Math.sin(this.a)*this.va*this.hbw/2
	}
	
	get vyl(){
		return this.vy + Math.cos(this.a)*this.va*this.hbw/2
	}
	
	get vyr(){
		return this.vy - Math.cos(this.a)*this.va*this.hbw/2
	}
	
	get speed(){
		return Math.sqrt(this.vx**2 + this.vy**2)
	}
	
	update(dt, ul, ur,  cfl, cfr, cfc){
		if(this.fuel <= 0 || this.broken){
			// lol
			ul = 0
			ur = 0
		}
		
		// total force
		let Fx = 0
		let Fy = 0
		
		// total moment
		let Mz = 0
		
		// contact forces
		Fx += cfl.x + cfr.x + cfc.x
		Fy += cfl.y + cfr.y + cfc.y
		
		// accumulate damage before friction
		let Fh = Math.sqrt(Fx**2 + Fy**2)
		if(Fh > this.damage_threshold){
			this.damage += Fh - this.damage_threshold
			this.damage = Math.min(100, this.damage)
		}
		
		if(this.damage >= 100){
			this.broken = true
			this.fuel = 0
		}
		
		// friction on contact
		if(Math.abs(Fx) > 0 || Math.abs(Fy) > 0){
			// wall direction is perpendicular to contact force direction
			let wfx = Fy/Fh
			let wfy = -Fx/Fh
			
			// determine component of velocity along this direction
			let dvw = this.vx*wfx + this.vy*wfy
			
			// add a 'friction' force in the opposite direction
			Fx += -dvw*wfx*this.k
			Fy += -dvw*wfy*this.k
		}
		
		// contact moments
		Mz += Math.sin(this.a)*(cfl.x - cfr.x) + Math.cos(this.a)*(cfl.y - cfr.y)
		
		// moment due to thrusters
		Mz += (ul - ur)*this.R - this.b*this.va*Math.abs(this.va)
		this.va += Mz*dt/this.I
		this.a += this.va*dt
		
		let thrust = (ul + ur)/this.m
		Fx += Math.sin(this.a)*thrust
		Fy += Math.cos(this.a)*thrust - this.m*this.g
		
		this.vx += Fx*dt/this.m
		this.vy += Fy*dt/this.m
		
		this.x = this.x + this.vx*dt
		this.y = this.y + this.vy*dt
		
		// systems
		this.fuel -= (ul + ur)/this.efficiency

		// energy, smoothed
		let K = this.vx**2 + this.vy**2 + this.va**2
		this.kinetic_energy = K*0.1 + this.kinetic_energy*0.9
	}
}

class Wall {
	constructor(x1, y1, x2, y2){
		this.x1 = x1
		this.x2 = x2
		this.y1 = y1
		this.y2 = y2
		this.stiffness = 5
		this.thickness = 5
		
		// keep track of objects that this wall might collide with
		// take note of which side of the wall they are on
		// prevent objects from going from one side to another unless
		// it goes around the wall
		// keys: some arbitrary string id
		// values: -1 for 'right' side, 0 for transition zone, 1 for 'left' side
		this.anticlip = {}
	}

	dist(x, y){
		let v12x = this.x2 - this.x1
		let v12y = this.y2 - this.y1
		let v1ox = x - this.x1
		let v1oy = y - this.y1
		let v2ox = x - this.x2
		let v2oy = y - this.y2
		if(v12x*v1ox + v12y*v1oy > 0 && v12x*v2ox + v12y*v2oy < 0){
			let dl = Math.abs(v12x*v1oy - v12y*v1ox)/Math.sqrt(v12x**2 + v12y**2)
			return dl
		}else{
			let d1 = Math.sqrt((x - this.x1)**2 + (y - this.y1)**2)
			let d2 = Math.sqrt((x - this.x2)**2 + (y - this.y2)**2)
			return Math.min(d1, d2)
		}
	}
	
	contactForce(x, y, vx, vy, R, id){
		let fx = 0
		let fy = 0
		
		let v12x = this.x2 - this.x1
		let v12y = this.y2 - this.y1
		let v12h = Math.sqrt(v12x**2 + v12y**2)
		let u12x = v12x/v12h
		let u12y = v12y/v12h

		let v1ox = x - this.x1
		let v1oy = y - this.y1
		let v1oh = Math.sqrt(v1ox**2 + v1oy**2)
		let u1ox = v1ox/v1oh
		let u1oy = v1oy/v1oh
		
		let v2ox = x - this.x2
		let v2oy = y - this.y2
		let v2oh = Math.sqrt(v2ox**2 + v2oy**2)
		let u2ox = v2ox/v2oh
		let u2oy = v2oy/v2oh
		
		let dp1 = u12x*u1ox + u12y*u1oy
		let dp2 = -u12x*u2ox - u12y*u2oy
		let cp1 = u12x*u1oy - u12y*u1ox
		// let cp2 = -u12x*u2oy + u12y*u2ox
		
		if(dp1 > 0 && dp2 > 0){
			// object is within normal collision zone
			let un12x = -u12y
			let un12y = u12x
			
			let side = Math.sign(cp1)
			let D = Math.abs(u12x*v1oy - v1ox*u12y)
			
			// anticlip?
			if(id in this.anticlip){
				if(this.anticlip[id] == 0 || this.anticlip[id] == side){
					// same sign as cache, no clipping happened, nothing to do
				}else{
					// opposite signs, do not allow clip
					side = -side
					D = 0
				}
			}
			this.anticlip[id] = side

			if(D <= R + this.thickness){
				let F = (R + this.thickness - D)*this.stiffness
				fx += un12x*side*F
				fy += un12y*side*F
			}
			
		}else{
			// object is in the transition zone, i.e. past the end of the wall
			this.anticlip[id] = 0
			
			// corner cases, when the object is close to the ends
			let d1 = Math.sqrt((x - this.x1)**2 + (y - this.y1)**2)
			if(d1 <= R + this.thickness){
				// apply half the force, since there is likely one other wall corner that it will contact
				// near the corners
				let F = (R + this.thickness - d1)*this.stiffness/2
				fx += (x - this.x1)/d1*F
				fy += (y - this.y1)/d1*F
			}
			
			let d2 = Math.sqrt((x - this.x2)**2 + (y - this.y2)**2)
			if(d2 <= R + this.thickness){
				// apply half the force, since there is likely one other wall corner that it will contact
				// near the corners
				let F = (R + this.thickness - d2)*this.stiffness/2
				fx += (x - this.x2)/d2*F
				fy += (y - this.y2)/d2*F
			}
		}
		
		// energy dissipation
		let dpv = (fx*vx + fy*vy)/(Math.sqrt(fx**2 + fy**2)*Math.sqrt(vx**2 + vy**2))
		if(dpv){
			fx *= (1 - 0.7*dpv)
			fy *= (1 - 0.7*dpv)
		}
		
		return {x: fx, y: fy}
	}
}

class Room {
	constructor(walls, spawn, goal){
		this.walls = walls
		this.spawn = spawn
		this.goal = goal
	}
	
	reset() {
		this.walls.forEach(wall => {
			wall.anticlip = {}
		})
	}
}

class Game {
	constructor(){
		this.room = undefined
		this.cleared = false
		this.drone = undefined
		this.dt = 0.2
	}
	
	reset(){
		this.drone = new Drone(this.room.spawn.x, this.room.spawn.y)
		this.room.reset()
		this.cleared = false
	}
	
	setLevel(room){
		this.room = room
		this.cleared = false
		this.reset()
	}
	
	update(ul, ur){
		// check collisions
		let collisions = []
		let cfl = {x: 0, y: 0}
		let cfc = {x: 0, y: 0}
		let cfr = {x: 0, y: 0}
		this.room.walls.forEach(wall => {
			if(wall.dist(this.drone.x, this.drone.y) < this.drone.hbw + this.drone.hbr + wall.thickness){
				// left collision
				let fl = wall.contactForce(this.drone.xl, this.drone.yl, this.drone.vxl, this.drone.vyl, this.drone.hbr, 'l')
				cfl.x += fl.x
				cfl.y += fl.y
				let Fl = Math.sqrt(fl.x**2 + fl.y**2)
				if(Fl > this.drone.damage_threshold){
					collisions.push({x: this.drone.xl, y: this.drone.yl, F: Fl})
				}
				
				// center collision
				let fc = wall.contactForce(this.drone.x, this.drone.y, this.drone.vx, this.drone.vy, this.drone.hbr, 'c')
				cfc.x += fc.x
				cfc.y += fc.y
				let Fc = Math.sqrt(fc.x**2 + fc.y**2)
				if(Fc > this.drone.damage_threshold){
					collisions.push({x: this.drone.x, y: this.drone.y, F: Fc})
				}
				
				// right collision
				let fr = wall.contactForce(this.drone.xr, this.drone.yr, this.drone.vxr, this.drone.vyr, this.drone.hbr, 'r')
				cfr.x += fr.x
				cfr.y += fr.y
				let Fr = Math.sqrt(fr.x**2 + fr.y**2)
				if(Fr > this.drone.damage_threshold){
					collisions.push({x: this.drone.xr, y: this.drone.yr, F: Fr})
				}
			}
		})
		
		this.drone.update(this.dt, ul, ur, cfl, cfr, cfc)
		
		// check win condition
		if(Math.sqrt((this.drone.x - this.room.goal.x)**2 + (this.drone.y - this.room.goal.y)**2) < this.room.goal.R/2
			&& this.drone.kinetic_energy < 0.01 && !this.drone.broken){
			this.cleared = true
		}

		return collisions
	}
}
