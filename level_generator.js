// Rocket Drone
// Copyright (C) 2022 wavy (wavy@optime.ca)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function generate_level(seed){
    randomSeed(seed)

    // generate path
    let start = { x: 0, y: 0, prev: undefined }
    let visited = []
    let min_dist = random(3, 9)
    let min_steps = random(5, 50)
    let path = [ start ]
    generate_path(start, visited, min_steps, path, min_dist)

    // create grid object
    let min_x = path.reduce((pv, cv) => Math.min(pv, cv.x), 0)
    let max_x = path.reduce((pv, cv) => Math.max(pv, cv.x), 0)
    let min_y = path.reduce((pv, cv) => Math.min(pv, cv.y), 0)
    let max_y = path.reduce((pv, cv) => Math.max(pv, cv.y), 0)

    let grid = []
    for(let gy = 0; gy <= (max_y - min_y); gy++){
        grid[gy] = []
        for(let gx = 0; gx <= (max_x - min_x); gx++){
            let pi = path.find(e => e.x == gx + min_x && e.y == gy + min_y)
            if(pi){
                grid[gy][gx] = ' '
            }else{
                grid[gy][gx] = 'x'
            }
        }
    }
    let empty = (gx, gy) => {
        return (gx >= 0 && gx <= max_x - min_x && gy >= 0 && gy <= max_y - min_y)
        && (grid[gy][gx] == ' ' || grid[gy][gx] == 'o')
    }
    
    // modify the grid in special cases to make the levels possible to beat
    let target = { x: path[path.length - 1].x - min_x, y: path[path.length - 1].y - min_y }
    if(!empty(target.x - 1, target.y) && !empty(target.x + 1, target.y) && !empty(target.x, target.y + 1)){
        // can only approach from the bottom...
        // not really doable in general
        // free up the left or right block too
        let bl = target.x == 0
        let br = target.x == max_x - min_x

        let dl = empty(target.x - 1, target.y - 1)
        let dr = empty(target.x + 1, target.y - 1)
        if(bl || br){
            if(bl){
                grid[target.y][target.x + 1] = ' '
            }else{
                grid[target.y][target.x - 1] = ' '
            }
        }else if(dl || dr){
            if(dl){
                grid[target.y][target.x - 1] = ' '
            }else{
                grid[target.y][target.x + 1] = ' '
            }
        }else{
            if(random(0, 10) > 5){
                grid[target.y][target.x + 1] = ' '
            }else{
                grid[target.y][target.x - 1] = ' '
            }
        }
    }
    
    // loop through the grid to create walls
    // scale variable
    let Z = random(180, 240)
    let walls = []
    let chamfers = []
    for(let gy = (max_y - min_y); gy >= 0; gy--){
        for(let gx = 0; gx <= (max_x - min_x); gx++){
            let gi = grid[gy][gx]
            if(gi == ' ' || gi == 'o'){
                let xu = (gy == max_y - min_y || grid[gy + 1][gx] == 'x')
                let xd = (gy == 0 || grid[gy - 1][gx] == 'x')
                let xl = (gx == 0 || grid[gy][gx - 1] == 'x')
                let xr = (gx == max_x - min_x || grid[gy][gx + 1] == 'x')

                // corner style - 1 chamfered, 2 square, otherwise none
                let c_ul = (xu && xl && random(0, 10) > 4) ? 1 : 2
                let c_ur = (xu && xr && random(0, 10) > 4) ? 1 : 2
                let c_dl = (xd && xl && random(0, 10) > 4) ? 1 : 2
                let c_dr = (xd && xr && random(0, 10) > 4) ? 1 : 2

                let obstructible = (path[0].x - min_x != gx && path[0].y - min_y != gy) 
                    && (target.x != gx && target.y != gy) && gi != 'o'

                // special case: narrow horizontal corridor
                if(obstructible && xu && xd && !xl && !xr && random(0, 10) > 6){
                    // narrow top?
                    let nt = random(0, 10) > 3
                    // narrow bottom?
                    let nb = random(0, 10) > 3

                    // sharp
                    if(random(0, 10) > 5){
                        // only top
                        if(nt && !nb){
                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z,
                                (gx + min_x)*Z, (gy + min_y)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x)*Z, (gy + min_y)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z
                            ))
                        }
                        // only bottom
                        else if(!nt && nb){
                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z,
                                (gx + min_x)*Z, (gy + min_y)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x)*Z, (gy + min_y)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z
                            ))
                        }
                        // both
                        else if(nt && nb){
                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z,
                                (gx + min_x)*Z, (gy + min_y + 0.2)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x)*Z, (gy + min_y + 0.2)*Z,
                                (gx + min_x)*Z, (gy + min_y + 0.2)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x)*Z, (gy + min_y + 0.2)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z,
                            ))

                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z,
                                (gx + min_x)*Z, (gy + min_y - 0.2)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x)*Z, (gy + min_y - 0.2)*Z,
                                (gx + min_x)*Z, (gy + min_y - 0.2)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x)*Z, (gy + min_y - 0.2)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z,
                            ))
                        }
                        // neither
                        else{
                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z
                            ))

                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z
                            ))
                        }
                    }
                    // square
                    else{
                        // only top
                        if(nt && !nb){
                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z,
                                (gx + min_x - 0.1)*Z, (gy + min_y + 0.5)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.1)*Z, (gy + min_y + 0.5)*Z,
                                (gx + min_x - 0.1)*Z, (gy + min_y)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.1)*Z, (gy + min_y)*Z,
                                (gx + min_x + 0.1)*Z, (gy + min_y)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.1)*Z, (gy + min_y)*Z,
                                (gx + min_x + 0.1)*Z, (gy + min_y + 0.5)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.1)*Z, (gy + min_y + 0.5)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z,
                            ))

                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z
                            ))
                        }
                        // only bottom
                        else if(!nt && nb){
                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z,
                                (gx + min_x - 0.1)*Z, (gy + min_y - 0.5)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.1)*Z, (gy + min_y - 0.5)*Z,
                                (gx + min_x - 0.1)*Z, (gy + min_y)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.1)*Z, (gy + min_y)*Z,
                                (gx + min_x + 0.1)*Z, (gy + min_y)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.1)*Z, (gy + min_y)*Z,
                                (gx + min_x + 0.1)*Z, (gy + min_y - 0.5)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.1)*Z, (gy + min_y - 0.5)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z,
                            ))

                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z
                            ))
                        }
                        // both
                        else if(nt && nb){
                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z,
                                (gx + min_x - 0.1)*Z, (gy + min_y + 0.5)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.1)*Z, (gy + min_y + 0.5)*Z,
                                (gx + min_x - 0.1)*Z, (gy + min_y + 0.2)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.1)*Z, (gy + min_y + 0.2)*Z,
                                (gx + min_x + 0.1)*Z, (gy + min_y + 0.2)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.1)*Z, (gy + min_y + 0.2)*Z,
                                (gx + min_x + 0.1)*Z, (gy + min_y + 0.5)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.1)*Z, (gy + min_y + 0.5)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z,
                            ))

                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z,
                                (gx + min_x - 0.1)*Z, (gy + min_y - 0.5)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.1)*Z, (gy + min_y - 0.5)*Z,
                                (gx + min_x - 0.1)*Z, (gy + min_y - 0.2)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.1)*Z, (gy + min_y - 0.2)*Z,
                                (gx + min_x + 0.1)*Z, (gy + min_y - 0.2)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.1)*Z, (gy + min_y - 0.2)*Z,
                                (gx + min_x + 0.1)*Z, (gy + min_y - 0.5)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.1)*Z, (gy + min_y - 0.5)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z,
                            ))
                        }
                        // neither
                        else{
                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z
                            ))

                            walls.push(new Wall(
                                (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z,
                                (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z
                            ))
                        }
                    }
                }
                // special case: narrow verical corridor
                else if(obstructible && !xu && !xd && xl && xr && empty(gx, gy-1) && empty(gx, gy-2)
                        && empty(gx, gy-3) && empty(gx, gy+1) && random(0, 10) > 8){

                    let nl = random(0, 10) > 3
                    let nr = random(0, 10) > 3

                    // clear the blocks below of obstructions
                    // this only works when the grid is iterated
                    // top to bottom
                    if(nl || nr){
                        let k = 1
                        while(k <= 3 && gy-k >= 0){
                            grid[gy-k][gx] = 'o'
                            k++
                        }
                    }

                    // sharp
                    if(random(0, 10) > 4){
                        // left only
                        if(nl && !nr){
                            walls.push(new Wall(
                                (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x - 0.1)*Z, (gy + min_y)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.1)*Z, (gy + min_y)*Z,
                                (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z,
                            ))

                            walls.push(new Wall(
                                (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z
                                ))
                            }
                        // right only
                        else if(!nl && nr){
                            walls.push(new Wall(
                                (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x + 0.1)*Z, (gy + min_y)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.1)*Z, (gy + min_y)*Z,
                                (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z,
                            ))

                            walls.push(new Wall(
                                (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z
                            ))
                        }
                        // both
                        else if(nl && nr){
                            walls.push(new Wall(
                                (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x - 0.2)*Z, (gy + min_y)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.2)*Z, (gy + min_y)*Z,
                                (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z,
                            ))

                            walls.push(new Wall(
                                (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x + 0.2)*Z, (gy + min_y)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.2)*Z, (gy + min_y)*Z,
                                (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z,
                            ))
                        }
                        // neither
                        else{
                            walls.push(new Wall(
                                (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z
                            ))
                        }
                    }
                    // square
                    else{
                        // left only
                        if(nl && !nr){
                            walls.push(new Wall(
                                (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x - 0.5)*Z, (gy + min_y - 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.5)*Z, (gy + min_y - 0.1)*Z,
                                (gx + min_x)*Z, (gy + min_y - 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x)*Z, (gy + min_y - 0.1)*Z,
                                (gx + min_x)*Z, (gy + min_y + 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x)*Z, (gy + min_y + 0.1)*Z,
                                (gx + min_x - 0.5)*Z, (gy + min_y + 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.5)*Z, (gy + min_y + 0.1)*Z,
                                (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z,
                            ))

                            walls.push(new Wall(
                                (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z
                                ))
                            }
                        // right only
                        else if(!nl && nr){
                            walls.push(new Wall(
                                (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x + 0.5)*Z, (gy + min_y - 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.5)*Z, (gy + min_y - 0.1)*Z,
                                (gx + min_x)*Z, (gy + min_y - 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x)*Z, (gy + min_y - 0.1)*Z,
                                (gx + min_x)*Z, (gy + min_y + 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x)*Z, (gy + min_y + 0.1)*Z,
                                (gx + min_x + 0.5)*Z, (gy + min_y + 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.5)*Z, (gy + min_y + 0.1)*Z,
                                (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z,
                            ))

                            walls.push(new Wall(
                                (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z
                            ))
                        }
                        // both
                        else if(nl && nr){
                            walls.push(new Wall(
                                (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x - 0.5)*Z, (gy + min_y - 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.5)*Z, (gy + min_y - 0.1)*Z,
                                (gx + min_x - 0.2)*Z, (gy + min_y - 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.2)*Z, (gy + min_y - 0.1)*Z,
                                (gx + min_x - 0.2)*Z, (gy + min_y + 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.2)*Z, (gy + min_y + 0.1)*Z,
                                (gx + min_x - 0.5)*Z, (gy + min_y + 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x - 0.5)*Z, (gy + min_y + 0.1)*Z,
                                (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z,
                            ))

                            walls.push(new Wall(
                                (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x + 0.5)*Z, (gy + min_y - 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.5)*Z, (gy + min_y - 0.1)*Z,
                                (gx + min_x + 0.2)*Z, (gy + min_y - 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.2)*Z, (gy + min_y - 0.1)*Z,
                                (gx + min_x + 0.2)*Z, (gy + min_y + 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.2)*Z, (gy + min_y + 0.1)*Z,
                                (gx + min_x + 0.5)*Z, (gy + min_y + 0.1)*Z,
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.5)*Z, (gy + min_y + 0.1)*Z,
                                (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z,
                            ))
                        }
                        // neither
                        else{
                            walls.push(new Wall(
                                (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z
                            ))
                            walls.push(new Wall(
                                (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                                (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z
                            ))
                        }
                    }
                }
                // special case: large chamfered corner - top left
                else if(obstructible && xu && xl && !xd && !xr && random(0, 10) > 7){
                    walls.push(new Wall(
                        (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                        (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z
                    ))
                    c_ul = 0
                }
                // special case: large chamfered corner - top right
                else if(obstructible && xu && xr && !xd && !xl && random(0, 10) > 7){
                    walls.push(new Wall(
                        (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                        (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z
                    ))
                    c_ur = 0
                }
                // special case: large chamfered corner - bottom left
                else if(obstructible && xd && xl && !xu && !xr && random(0, 10) > 7){
                    walls.push(new Wall(
                        (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z,
                        (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z
                    ))
                    c_dl = 0
                }
                // special case: large chamfered corner - bottom right
                else if(obstructible && xd && xr && !xu && !xl && random(0, 10) > 7){
                    walls.push(new Wall(
                        (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z,
                        (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z
                    ))
                    c_dr = 0
                }
                // special case: center obstacle
                else if(obstructible && !xu && !xd && !xl && !xr && random(0, 10) > 8){
                    let type = random(0, 10)
                    if(type < 5){
                        walls.push(new Wall(
                            (gx + min_x - 0.15)*Z, (gy + min_y - 0.15)*Z,
                            (gx + min_x + 0.15)*Z, (gy + min_y - 0.15)*Z
                        ))
                        walls.push(new Wall(
                            (gx + min_x + 0.15)*Z, (gy + min_y - 0.15)*Z,
                            (gx + min_x + 0.15)*Z, (gy + min_y + 0.15)*Z
                        ))
                        walls.push(new Wall(
                            (gx + min_x + 0.15)*Z, (gy + min_y + 0.15)*Z,
                            (gx + min_x - 0.15)*Z, (gy + min_y + 0.15)*Z
                        ))
                        walls.push(new Wall(
                            (gx + min_x - 0.15)*Z, (gy + min_y + 0.15)*Z,
                            (gx + min_x - 0.15)*Z, (gy + min_y - 0.15)*Z
                        ))
                    }else{
                        walls.push(new Wall(
                            (gx + min_x - 0.2)*Z, (gy + min_y)*Z,
                            (gx + min_x)*Z, (gy + min_y - 0.2)*Z
                        ))
                        walls.push(new Wall(
                            (gx + min_x)*Z, (gy + min_y - 0.2)*Z,
                            (gx + min_x + 0.2)*Z, (gy + min_y)*Z
                        ))
                        walls.push(new Wall(
                            (gx + min_x + 0.2)*Z, (gy + min_y)*Z,
                            (gx + min_x)*Z, (gy + min_y + 0.2)*Z
                        ))
                        walls.push(new Wall(
                            (gx + min_x)*Z, (gy + min_y + 0.2)*Z,
                            (gx + min_x - 0.2)*Z, (gy + min_y)*Z
                        ))
                    }
                }
                // special case: single wall obstacles - top
                else if(obstructible && xu && !xd && !xl && !xr && random(0, 10) > 7){
                    if(random(0, 10) > 5){
                        // sharp
                        walls.push(new Wall(
                            (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z,
                            (gx + min_x)*Z, (gy + min_y)*Z
                        ))
                        walls.push(new Wall(
                            (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z,
                            (gx + min_x)*Z, (gy + min_y)*Z
                        ))
                    }else{
                        // square
                        walls.push(new Wall(
                            (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z,
                            (gx + min_x - 0.1)*Z, (gy + min_y + 0.5)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x - 0.1)*Z, (gy + min_y + 0.5)*Z,
                            (gx + min_x - 0.1)*Z, (gy + min_y)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x - 0.1)*Z, (gy + min_y)*Z,
                            (gx + min_x + 0.1)*Z, (gy + min_y)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x + 0.1)*Z, (gy + min_y)*Z,
                            (gx + min_x + 0.1)*Z, (gy + min_y + 0.5)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x + 0.1)*Z, (gy + min_y + 0.5)*Z,
                            (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z,
                        ))
                    }
                }
                // special case: single wall obstacles - bottom
                else if(obstructible && !xu && xd && !xl && !xr && random(0, 10) > 7){
                    if(random(0, 10) > 5){
                        // sharp
                        walls.push(new Wall(
                            (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z,
                            (gx + min_x)*Z, (gy + min_y)*Z
                        ))
                        walls.push(new Wall(
                            (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z,
                            (gx + min_x)*Z, (gy + min_y)*Z
                        ))
                    }else{
                        // square
                        walls.push(new Wall(
                            (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z,
                            (gx + min_x - 0.1)*Z, (gy + min_y - 0.5)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x - 0.1)*Z, (gy + min_y - 0.5)*Z,
                            (gx + min_x - 0.1)*Z, (gy + min_y)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x - 0.1)*Z, (gy + min_y)*Z,
                            (gx + min_x + 0.1)*Z, (gy + min_y)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x + 0.1)*Z, (gy + min_y)*Z,
                            (gx + min_x + 0.1)*Z, (gy + min_y - 0.5)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x + 0.1)*Z, (gy + min_y - 0.5)*Z,
                            (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z,
                        ))
                    }
                }
                // special case: single wall obstacles - left
                else if(obstructible && !xu && !xd && xl && !xr && random(0, 10) > 7){
                    if(random(0, 10) > 5){
                        // sharp
                        walls.push(new Wall(
                            (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                            (gx + min_x)*Z, (gy + min_y)*Z
                        ))
                        walls.push(new Wall(
                            (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z,
                            (gx + min_x)*Z, (gy + min_y)*Z
                        ))
                    }else{
                        // square
                        walls.push(new Wall(
                            (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                            (gx + min_x - 0.5)*Z, (gy + min_y - 0.1)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x - 0.5)*Z, (gy + min_y - 0.1)*Z,
                            (gx + min_x)*Z, (gy + min_y - 0.1)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x)*Z, (gy + min_y - 0.1)*Z,
                            (gx + min_x)*Z, (gy + min_y + 0.1)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x)*Z, (gy + min_y + 0.1)*Z,
                            (gx + min_x - 0.5)*Z, (gy + min_y + 0.1)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x - 0.5)*Z, (gy + min_y + 0.1)*Z,
                            (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z,
                        ))
                    }
                }
                // special case: single wall obstacles - right
                else if(obstructible && !xu && !xd && !xl && xr && random(0, 10) > 7){
                    if(random(0, 10) > 5){
                        // sharp
                        walls.push(new Wall(
                            (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                            (gx + min_x)*Z, (gy + min_y)*Z
                        ))
                        walls.push(new Wall(
                            (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z,
                            (gx + min_x)*Z, (gy + min_y)*Z
                        ))
                    }else{
                        // square
                        walls.push(new Wall(
                            (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                            (gx + min_x + 0.5)*Z, (gy + min_y - 0.1)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x + 0.5)*Z, (gy + min_y - 0.1)*Z,
                            (gx + min_x)*Z, (gy + min_y - 0.1)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x)*Z, (gy + min_y - 0.1)*Z,
                            (gx + min_x)*Z, (gy + min_y + 0.1)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x)*Z, (gy + min_y + 0.1)*Z,
                            (gx + min_x + 0.5)*Z, (gy + min_y + 0.1)*Z,
                        ))
                        walls.push(new Wall(
                            (gx + min_x + 0.5)*Z, (gy + min_y + 0.1)*Z,
                            (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z,
                        ))
                    }
                }
                // general case
                else{
                    // faces
                    if(xu){
                        walls.push(new Wall(
                            (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z,
                            (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z
                        ))
                    }
                    if(xd){
                        walls.push(new Wall(
                            (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z,
                            (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z
                        ))
                    }
                    if(xl){
                        walls.push(new Wall(
                            (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                            (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z
                        ))
                    }
                    if(xr){
                        walls.push(new Wall(
                            (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                            (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z
                        ))
                    }
                }

                // default chamfered corners
                if(c_ul == 1){
                    walls.push(new Wall(
                        (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z,
                        (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z
                    ))
                }
                if(c_dl == 1){
                    walls.push(new Wall(
                        (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                        (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z
                    ))
                }
                if(c_ur == 1){
                    walls.push(new Wall(
                        (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z,
                        (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z
                    ))
                }
                if(c_dr == 1){
                    walls.push(new Wall(
                        (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                        (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z
                    ))
                }

                // chamfers or inner corners
                let ch_ul = empty(gx - 1, gy + 1) && empty(gx - 1, gy) && !empty(gx, gy + 1)
                let ch_ur = empty(gx + 1, gy + 1) && empty(gx + 1, gy) && !empty(gx, gy + 1)
                let ch_lu = empty(gx - 1, gy + 1) && empty(gx, gy + 1) && !empty(gx - 1, gy)
                let ch_ld = empty(gx - 1, gy - 1) && empty(gx, gy - 1) && !empty(gx - 1, gy)
                let ch_ru = empty(gx + 1, gy + 1) && empty(gx, gy + 1) && !empty(gx + 1, gy)
                let ch_rd = empty(gx + 1, gy - 1) && empty(gx, gy - 1) && !empty(gx + 1, gy)
                let ch_dl = empty(gx - 1, gy - 1) && empty(gx - 1, gy) && !empty(gx, gy - 1)
                let ch_dr = empty(gx + 1, gy - 1) && empty(gx + 1, gy) && !empty(gx, gy - 1)

                if(ch_ul && !chamfers.find(c => c.x == gx && c.y == gy + 1 && c.s == 4)){
                    chamfers.push({x: gx, y: gy + 1, s: 4})
                }
                if(ch_ur && !chamfers.find(c => c.x == gx && c.y == gy + 1 && c.s == 3)){
                    chamfers.push({x: gx, y: gy + 1, s: 3})
                }
                if(ch_dl && !chamfers.find(c => c.x == gx && c.y == gy - 1 && c.s == 1)){
                    chamfers.push({x: gx, y: gy - 1, s: 1})
                }
                if(ch_dr && !chamfers.find(c => c.x == gx && c.y == gy - 1 && c.s == 2)){
                    chamfers.push({x: gx, y: gy - 1, s: 2})
                }
                if(ch_lu && !chamfers.find(c => c.x == gx - 1 && c.y == gy && c.s == 2)){
                    chamfers.push({x: gx - 1, y: gy, s: 2})
                }
                if(ch_ld && !chamfers.find(c => c.x == gx - 1 && c.y == gy && c.s == 3)){
                    chamfers.push({x: gx - 1, y: gy, s: 3})
                }
                if(ch_ru && !chamfers.find(c => c.x == gx + 1 && c.y == gy && c.s == 1)){
                    chamfers.push({x: gx + 1, y: gy, s: 1})
                }
                if(ch_rd && !chamfers.find(c => c.x == gx + 1 && c.y == gy && c.s == 4)){
                    chamfers.push({x: gx + 1, y: gy, s: 4})
                }

                // finish square corners
                if(c_ul == 2 && !ch_lu && !ch_ul){
                    if(xu){
                        walls.push(new Wall(
                            (gx + min_x - 0.5)*Z, (gy + min_y + 0.5)*Z,
                            (gx + min_x - 0.3)*Z, (gy + min_y + 0.5)*Z
                        ))
                    }
                    if(xl){
                        walls.push(new Wall(
                            (gx + min_x - 0.5)*Z, (gy + min_y + 0.3)*Z,
                            (gx + min_x - 0.5)*Z, (gy + min_y + 0.5)*Z
                        ))
                    }
                }
                if(c_ur == 2 && !ch_ur && !ch_ru){
                    if(xu){
                        walls.push(new Wall(
                            (gx + min_x + 0.5)*Z, (gy + min_y + 0.5)*Z,
                            (gx + min_x + 0.3)*Z, (gy + min_y + 0.5)*Z
                            ))
                    }
                    if(xr){
                        walls.push(new Wall(
                            (gx + min_x + 0.5)*Z, (gy + min_y + 0.3)*Z,
                            (gx + min_x + 0.5)*Z, (gy + min_y + 0.5)*Z
                        ))
                    }
                }
                if(c_dl == 2 && !ch_ld && !ch_dl){
                    if(xd){
                        walls.push(new Wall(
                            (gx + min_x - 0.5)*Z, (gy + min_y - 0.5)*Z,
                            (gx + min_x - 0.3)*Z, (gy + min_y - 0.5)*Z
                        ))
                    }
                    if(xl){
                        walls.push(new Wall(
                            (gx + min_x - 0.5)*Z, (gy + min_y - 0.3)*Z,
                            (gx + min_x - 0.5)*Z, (gy + min_y - 0.5)*Z
                        ))
                    }
                }
                if(c_dr == 2 && !ch_rd && !ch_dr){
                    if(xd){
                        walls.push(new Wall(
                            (gx + min_x + 0.5)*Z, (gy + min_y - 0.5)*Z,
                            (gx + min_x + 0.3)*Z, (gy + min_y - 0.5)*Z
                        ))
                    }
                    if(xr){
                        walls.push(new Wall(
                            (gx + min_x + 0.5)*Z, (gy + min_y - 0.3)*Z,
                            (gx + min_x + 0.5)*Z, (gy + min_y - 0.5)*Z
                        ))
                    }
                }
            }
        }
    }

    // fill chamfers
    chamfers.forEach(ch => {
        if(random(0, 10) > 5){
            if(ch.s == 1){
                // top left
                walls.push(new Wall(
                    (ch.x + min_x - 0.5)*Z, (ch.y + min_y + 0.3)*Z,
                    (ch.x + min_x - 0.3)*Z, (ch.y + min_y + 0.5)*Z
                ))
            }else if(ch.s == 2){
                // top right
                walls.push(new Wall(
                    (ch.x + min_x + 0.5)*Z, (ch.y + min_y + 0.3)*Z,
                    (ch.x + min_x + 0.3)*Z, (ch.y + min_y + 0.5)*Z
                ))
            }else if(ch.s == 3){
                // bot right
                walls.push(new Wall(
                    (ch.x + min_x + 0.5)*Z, (ch.y + min_y - 0.3)*Z,
                    (ch.x + min_x + 0.3)*Z, (ch.y + min_y - 0.5)*Z
                ))
            }else if(ch.s == 4){
                // bot left
                walls.push(new Wall(
                    (ch.x + min_x - 0.5)*Z, (ch.y + min_y - 0.3)*Z,
                    (ch.x + min_x - 0.3)*Z, (ch.y + min_y - 0.5)*Z
                ))
            }
        }else{
            if(ch.s == 1){
                // top left
                walls.push(new Wall(
                    (ch.x + min_x - 0.5)*Z, (ch.y + min_y + 0.3)*Z,
                    (ch.x + min_x - 0.5)*Z, (ch.y + min_y + 0.5)*Z
                ))
                walls.push(new Wall(
                    (ch.x + min_x - 0.5)*Z, (ch.y + min_y + 0.5)*Z,
                    (ch.x + min_x - 0.3)*Z, (ch.y + min_y + 0.5)*Z
                ))
            }else if(ch.s == 2){
                // top right
                walls.push(new Wall(
                    (ch.x + min_x + 0.5)*Z, (ch.y + min_y + 0.3)*Z,
                    (ch.x + min_x + 0.5)*Z, (ch.y + min_y + 0.5)*Z
                ))
                walls.push(new Wall(
                    (ch.x + min_x + 0.5)*Z, (ch.y + min_y + 0.5)*Z,
                    (ch.x + min_x + 0.3)*Z, (ch.y + min_y + 0.5)*Z
                ))
            }else if(ch.s == 3){
                // bot right
                walls.push(new Wall(
                    (ch.x + min_x + 0.5)*Z, (ch.y + min_y - 0.3)*Z,
                    (ch.x + min_x + 0.5)*Z, (ch.y + min_y - 0.5)*Z
                ))
                walls.push(new Wall(
                    (ch.x + min_x + 0.5)*Z, (ch.y + min_y - 0.5)*Z,
                    (ch.x + min_x + 0.3)*Z, (ch.y + min_y - 0.5)*Z
                ))
            }else if(ch.s == 4){
                // bot left
                walls.push(new Wall(
                    (ch.x + min_x - 0.5)*Z, (ch.y + min_y - 0.3)*Z,
                    (ch.x + min_x - 0.5)*Z, (ch.y + min_y - 0.5)*Z
                ))
                walls.push(new Wall(
                    (ch.x + min_x - 0.5)*Z, (ch.y + min_y - 0.5)*Z,
                    (ch.x + min_x - 0.3)*Z, (ch.y + min_y - 0.5)*Z
                ))
            }
        }
    })

    // create the spawn and goal points
    let spawn = { x: 0, y: 0 }
    if(!empty(-min_x, -min_y-1) && random(0, 10) > 2){
        // can spawn on ground
        spawn.y = -Z/2 + 15
    }else{
        // must spawn on platform
        // spawn platform
        walls.push(new Wall(spawn.x - Z/4, spawn.y - 15, spawn.x + Z/4, spawn.y - 15))
        walls.push(new Wall(spawn.x - Z/4, spawn.y - 15, spawn.x - Z/4, spawn.y - Z/5))
        walls.push(new Wall(spawn.x - Z/4, spawn.y - Z/5, spawn.x + Z/4, spawn.y - Z/5))
        walls.push(new Wall(spawn.x + Z/4, spawn.y - 15, spawn.x + Z/4, spawn.y - Z/5))
    }

    let goal = { x: (target.x + min_x)*Z, y: (target.y + min_y)*Z, R: 30 }
    if(!empty(target.x, target.y - 1) && random(0, 10) > 2){
        // can finish on ground
        goal.y += -Z/2 + 15
    }else{
        // must finish on platform
        // goal platform
        walls.push(new Wall(goal.x - Z/4, goal.y - 15, goal.x + Z/4, goal.y - 15))
        walls.push(new Wall(goal.x - Z/4, goal.y - 15, goal.x - Z/4, goal.y - Z/5))
        walls.push(new Wall(goal.x - Z/4, goal.y - Z/5, goal.x + Z/4, goal.y - Z/5))
        walls.push(new Wall(goal.x + Z/4, goal.y - 15, goal.x + Z/4, goal.y - Z/5))
    }

    return new Room(walls, spawn, goal)
}

function unvisited_neighbors(p, visited){
    let xu = visited.some(o => (o.x == p.x && o.y == p.y+1))
    let xd = visited.some(o => (o.x == p.x && o.y == p.y-1))
    let xl = visited.some(o => (o.x == p.x-1 && o.y == p.y))
    let xr = visited.some(o => (o.x == p.x+1 && o.y == p.y))
    let neighbors = []
    if(!xu){
        neighbors.push({ x: p.x, y: p.y+1 })
    }
    if(!xd){
        neighbors.push({ x: p.x, y: p.y-1 })
    }
    if(!xl){
        neighbors.push({ x: p.x-1, y: p.y })
    }
    if(!xr){
        neighbors.push({ x: p.x+1, y: p.y })
    }
    return neighbors
}

function generate_path(here, visited, steps_remaining, path, min_dist){
    let dist = path.length > 0 ? Math.abs(here.x - path[0].x) + Math.abs(here.y - path[0].y) : 0
    if(steps_remaining <= 0 && dist > min_dist){
        return path
    }else{
        let p = here
        visited.push(here)
        let np = unvisited_neighbors(p, visited)
        while(np.length == 0){
            if(p.prev){
                p = p.prev
                np = unvisited_neighbors(p, visited)
            }else{
                return path
            }
        }
        let next = np[Math.floor(random(0, np.length))]
        next.prev = p
        path.push(next)
        return generate_path(next, visited, steps_remaining - 1, path, min_dist)
    }
}