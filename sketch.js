// Rocket Drone
// Copyright (C) 2022 wavy (wavy@optime.ca)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const is_mobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)
const V1_MAX_SEED = 9999999999

// URL query strings
const params = new URLSearchParams(location.search)
// level seed
let curr_level = parseInt(params.get('level'))
// encoded replay
let replay_str = params.get('replay')
let saved_replay = replay_str ? LZString.decompressFromEncodedURIComponent(replay_str) : undefined
let replay = new Replay(saved_replay)

const game = new Game()

const particle_system = new ParticleSystem()
const TRAIL_SPD = 20

let text_font
function preload() {
	text_font = loadFont('FFCaronBulgeBold.ttf')
}

// DOM overlay objects
let fuel_meter
let health_meter
let hud
let level_text
let time_text
let reset_text
let replay_text
let left_text
let right_text
let menu_text
let overlay
let info_splash

// this hack is needed for the ios web-app mode,
// which doesn't handle screen rotation correctly
// see my own post at https://discourse.processing.org/t/fullscreen-web-app-ios-orientation-bug/28734/3
function hackResized(){
	let rect = hud.getBoundingClientRect()
	resizeCanvas(rect.width, rect.height)
}

// game states
let show_title = true
let t = 0
let t_start = 0
let replay_mode = false
let daily_mode = false

// input states
let thrust_left = false
let thrust_right = false

// camera shake states
let camera_shake_x = 0
let camera_shake_y = 0
let camera_shake_z = 0
let camera_shake_vx = 0
let camera_shake_vy = 0
let camera_shake_vz = 0

// a lowpass filtered speed value, for use in camera zoom
let lpf_speed = 0

function startGame(level){
	show_title = false
	if(level){
		if(level > 0 && level <= tutorial_rooms.length){
			game.setLevel(tutorial_rooms[level - 1])
		}else{
			game.setLevel(generate_level(level))
		}
	}else{
		level = Math.floor(random(100, V1_MAX_SEED))
		game.setLevel(generate_level(level))
	}

	curr_level = level
	resetRoom()

	// replace query string
	params.set('level', level)
	window.history.replaceState({}, '', `${location.pathname}?${params.toString()}`)
}

function resetRoom(){
	game.reset()
	replay_mode = false
	t = 0
	t_start = Infinity
	particle_system.clear()

	// DOM overlay reset
	hud.classList.remove('hidden')
	level_text.innerHTML = 'Level ' + curr_level
	reset_text.innerHTML = ''
	replay_text.innerHTML = ''
	reset_text.innerHTML = is_mobile ? 'RESET' : ''
	left_text.innerHTML = is_mobile ? 'LEFT<br>THRUST' : ''
	right_text.innerHTML = is_mobile ? 'RIGHT<br>THRUST' : ''
	menu_text.innerHTML = is_mobile ? 'MENU' : 'ESC: Menu'
	overlay.classList.add('hidden')

	// Camera reset
	camera_shake_x = 0
	camera_shake_y = 0
	camera_shake_z = 0
	camera_shake_vx = 0
	camera_shake_vy = 0
	camera_shake_vz = 0

	// RNG reset
	randomSeed(Math.floor(Math.random() * Number.MAX_SAFE_INTEGER))
}

function nextLevel(level){
	// move on to the next level
	level += 1
	if(level > 0 && level <= tutorial_rooms.length){
		game.setLevel(tutorial_rooms[level - 1])
	}else if(level == tutorial_rooms.length + 1){
		backToMenu()
		level = undefined
		return
	}else{
		game.setLevel(generate_level(level))
	}

	curr_level = level
	resetRoom()

	// replace query string
	params.set('level', level)
	window.history.replaceState({}, '', `${location.pathname}?${params.toString()}`)
}

function backToMenu(){
	show_title = true
	daily_mode = false
	curr_level = undefined
	game.room = undefined
	game.cleared = false
	game.drone = undefined
	
	// camera reset
	camera_shake_x = 0
	camera_shake_y = 0
	camera_shake_z = 0
	camera_shake_vx = 0
	camera_shake_vy = 0
	camera_shake_vz = 0
	// reset camera position so the menu draws correctly
	camera()

	particle_system.clear()

	// DOM reset
	hud.classList.add('hidden')
	level_text.innerHTML = 'Level ' + game.level
	replay_text.innerHTML = ''
	reset_text.innerHTML = ''
	left_text.innerHTML = ''
	right_text.innerHTML = ''
	menu_text.innerHTML = ''
	overlay.classList.remove('hidden')

	replay_mode = false
	replay.clear()

	// replace query string and remove level and replay strings
	window.history.replaceState({}, '', `${location.pathname}`)
}

function getDailySeed(){
	let xml_http = new XMLHttpRequest()
	xml_http.open('HEAD', window.location.href.toString(), false)
	xml_http.setRequestHeader("Content-Type", "text/html")
	xml_http.send('')
	let day_start = new Date(xml_http.getResponseHeader("Date"))
	day_start.setUTCHours(0,0,0,0)
	randomSeed(day_start.getTime())
	return Math.floor(random(1000, V1_MAX_SEED))
}

function startReplay(){
	replay_text.innerHTML = 'Replay Mode'
	reset_text.innerHTML = is_mobile ? '' : 'Press Enter to continue'
	left_text.innerHTML = ''
	right_text.innerHTML = is_mobile ? 'CONTINUE' : ''

	replay_mode = true
	game.reset()
	particle_system.clear()
	t = 0

	// save replay as query string
	replay_str = LZString.compressToEncodedURIComponent(replay.cereal())

	// replace query string
	params.set('replay', replay_str)
	window.history.replaceState({}, '', `${location.pathname}?${params.toString()}`)
}

function clearReplay(){
	replay.clear()
	params.delete('replay')
	window.history.replaceState({}, '', `${location.pathname}?${params.toString()}`)
}

function setup(){
	let canvas = createCanvas(windowWidth, windowHeight, WEBGL)
	canvas.position(0, 0)
	canvas.style('z-index', '-1')
	frameRate(60)

	hud = document.getElementById('hud')
	fuel_meter = document.getElementById('fuel')
	health_meter = document.getElementById('health')
	level_text = document.getElementById('level')
	time_text = document.getElementById('time')
	reset_text = document.getElementById('reset')
	replay_text = document.getElementById('replay')
	left_text = document.getElementById('left')
	right_text = document.getElementById('right')
	menu_text = document.getElementById('menu')
	overlay = document.getElementById('overlay')
	info_splash = document.getElementById('info')
	if(is_mobile){
		left_text.classList.remove('hidden')
		right_text.classList.remove('hidden')
	}
	new ResizeObserver(hackResized).observe(hud)

	if(curr_level){
		// level is already defined
		startGame(curr_level)

		if(saved_replay){
			replay_text.innerHTML = 'Replay Mode'
			left_text.innerHTML = ''
			reset_text.innerHTML = is_mobile ? '' : 'Press Enter to start'
			right_text.innerHTML = is_mobile ? 'START' : ''
			game.cleared = true
			replay_mode = true
			time_text.innerHTML = ((replay.end_time - replay.start_time)/62.5).toFixed(2)
		}
	}else{
		backToMenu()
	}

	// 16ms update interval is a bit faster than 60 fps, but i prefer integers
	next_game_tick_time = Date.now() + 16
	setTimeout(gameLoop, 16)
}

let next_game_tick_time = 0
function gameLoop(){
	let now = Date.now()
	let timing_error = now - next_game_tick_time
	if(focused && timing_error < 32){
		next_game_tick_time += 16
	}else{
		next_game_tick_time = now + 16
	}
	setTimeout(gameLoop, 16 - timing_error)

	if(show_title){
		return
	}

	t++

	let ul = 0
	let ur = 0

	if(replay_mode){
		let replay_move = replay.at(t)
		ul = replay_move.l
		ur = replay_move.r
	}else{
		// Q
		if(thrust_left){
			// add thrust left
			ul = 1
			if(t_start > t){
				t_start = t
			}
		}
		// P
		if(thrust_right){
			// add thrust right
			ur = 1
			if(t_start > t){
				t_start = t
			}
		}
		if(!game.cleared && !game.drone.broken){
			replay.add(t, ul, ur)
		}
	}
	
	if(ul > 0 && game.drone.fuel > 0 && !game.drone.broken){
		// spawn particle
		let pvx = game.drone.vxl - Math.sin(game.drone.a)*(TRAIL_SPD + random(0, 5)) + Math.cos(game.drone.a)*randomGaussian(0,3)
		let pvy = game.drone.vyl - Math.cos(game.drone.a)*(TRAIL_SPD + random(0, 5)) + Math.sin(game.drone.a)*randomGaussian(0,3)
		let T = 4 + random(0, 2)
		particle_system.add(game.drone.xl, game.drone.yl, pvx, pvy, T, '#d48e15')
	}
	
	if(ur > 0 && game.drone.fuel > 0 && !game.drone.broken){
		// spawn particle
		let pvx = game.drone.vxr - Math.sin(game.drone.a)*(TRAIL_SPD + random(0, 5)) + Math.cos(game.drone.a)*randomGaussian(0,3)
		let pvy = game.drone.vyr - Math.cos(game.drone.a)*(TRAIL_SPD + random(0, 5)) + Math.sin(game.drone.a)*randomGaussian(0,3)
		let T = 4 + random(0, 2)
		particle_system.add(game.drone.xr, game.drone.yr, pvx, pvy, T, '#d48e15')
	}
	
	// game state changes
	let was_cleared = game.cleared
	let was_broken = game.drone.broken
	let collisions = game.update(ul, ur)
	let now_broken = game.drone.broken
	let now_cleared = game.cleared

	// create collision particles
	collisions.forEach(col => {
		for(let i = 0; i < Math.sqrt(col.F); i++){
			particle_system.add(col.x, col.y, random(-10,10), random(-10, 10), 3 + random(0, 3), now_broken ? '#f37329' : '#e4c6fa')
		}
	})

	// explosion
	if(!was_broken && now_broken){
		for(let i = 0; i < 30; i++){
			let pvx = game.drone.vx + random(-1, 1)*random(3, 30)
			let pvy = game.drone.vy + random(-1, 1)*random(3, 30)
			let pc = color(240 + random(-100, 15), 100 + random(-50, 50), 50 + random(-30, 30))
			particle_system.add(game.drone.x, game.drone.y, pvx, pvy, 10 + random(0, 5), pc)
		}
		for(let i = 0; i < 30; i++){
			let pvx = game.drone.vxr + random(-1, 1)*random(3, 30)
			let pvy = game.drone.vyr + random(-1, 1)*random(3, 30)
			let pc = color(240 + random(-100, 15), 100 + random(-50, 50), 50 + random(-30, 30))
			particle_system.add(game.drone.xr, game.drone.yr, pvx, pvy, 10 + random(0, 5), pc)
		}
		for(let i = 0; i < 30; i++){
			let pvx = game.drone.vxl + random(-1, 1)*random(3, 30)
			let pvy = game.drone.vyl + random(-1, 1)*random(3, 30)
			let pc = color(240 + random(-100, 15), 100 + random(-50, 50), 50 + random(-30, 30))
			particle_system.add(game.drone.xl, game.drone.yl, pvx, pvy, 10 + random(0, 5), pc)
		}
		camera_shake_vx = random(-10, 10)
		camera_shake_vy = random(-10, 10)
		camera_shake_vz = random(-10, 10)
		camera_shake_x = random(-50, 50)
		camera_shake_y = random(-50, 50)
		camera_shake_z = random(-50, 50)

		reset_text.innerHTML = is_mobile ? 'RESET' : 'Press Backspace to reset'
		left_text.innerHTML = ''
		right_text.innerHTML = ''
	}

	// particle update
	particle_system.update()
		
	// camera state updates
	camera_shake_vx += -random(0.01, 0.1)*camera_shake_vx - random(0.01, 0.1)*camera_shake_x
	camera_shake_vy += -random(0.01, 0.1)*camera_shake_vy - random(0.01, 0.1)*camera_shake_y
	camera_shake_vz += -random(0.01, 0.1)*camera_shake_vz - random(0.01, 0.1)*camera_shake_z
	camera_shake_x += camera_shake_vx
	camera_shake_y += camera_shake_vy
	camera_shake_z += camera_shake_vz
	lpf_speed = 0.025*game.drone.speed + 0.975*lpf_speed

	if(replay_mode){
		// loop replay
		if(t >= replay.end_time + 60){
			game.reset()
			particle_system.clear()
			t = 0
		}
	}else{
		// was_cleared instead of now_cleared, so that the time left on screen will be
		// consistent with the replay time
		if(!was_cleared){
			time_text.innerHTML = ((t - Math.min(t_start, t))/62.5).toFixed(2)
		}
	
		if(!was_cleared && now_cleared){
			replay.end(t_start, t)
			replay_text.innerHTML = is_mobile ? '' : 'Press Space for replay'
			reset_text.innerHTML = is_mobile ? 'RESET' : 'Press Enter to continue'
			left_text.innerHTML = is_mobile ? 'REPLAY' : ''
			right_text.innerHTML = is_mobile ? 'CONTINUE' : ''
		}
	}
}

function draw(){
	if(show_title){
		background(26)
		textFont(text_font)
		textAlign(CENTER, CENTER)
		let ss = (width + height)/2
		if(is_mobile){
			textSize(ss / 10)
			text('ROCKET DRONE', 0, -height/6)
			textSize(ss / 25)
			text('TUTORIAL', -width/3, height/6)
			text('FREE PLAY', 0, height/6)
			text('DAILY', width/3, height/6)
		}else{
			textSize(ss / 15)
			text('ROCKET DRONE', 0, -height/4)
			textSize(ss / 40)
			text('Use Q and P to Play', 0, 0)
			text('Press Enter for free play', 0, height/10)
			text('Press T for tutorial', 0, height/20)
			text('Press D for daily challenge', 0, height*3/20)
		}
	}else{
		camera(game.drone.x, -game.drone.y, Math.min(height*1.5, 800) + lpf_speed*20 + camera_shake_z, 
				game.drone.x + camera_shake_x, -game.drone.y + camera_shake_y, 0)

		// draw
		background(26)
		applyMatrix(1, 0, 0, -1, 0, 0)
	
		// draw particles
		push()
		noFill()
		particle_system.particles.forEach(p => {
			strokeWeight(2)
			stroke(p.c)
			circle(p.x, p.y, p.T)
		})
		pop()
		
		// draw goal zone
		push()
		rectMode(CENTER)
		if((replay_mode && t >= replay.end_time) || (!replay_mode && game.cleared)){
			fill('#3a9104')
		}else{
			fill('#667885')
		}
		noStroke()
		rect(game.room.goal.x, game.room.goal.y, game.room.goal.R+game.drone.hbw, game.drone.hbh+game.room.goal.R/2)
		pop()
		
		// draw walls
		push()
		strokeWeight(5)
		stroke('#7e8087')
		game.room.walls.forEach(wall => {
			line(wall.x1, wall.y1, wall.x2, wall.y2)
		})
		noStroke()
		fill('#7e8087')
		game.room.walls.forEach(wall => {
			circle(wall.x1, wall.y1, wall.thickness)
			circle(wall.x2, wall.y2, wall.thickness)
		})
		pop()
		
		// draw drone
		push()
	
		// broken coloring
		if(game.drone.broken){
			fill('#715344')
		}else{
			fill('#fafafa')
		}
	
		noStroke()
		rectMode(CENTER)
		translate(game.drone.x, game.drone.y,1)
		rotate(-game.drone.a)
	
		// arm
		rect(0, 0, 60, 4)
	
		// body
		circle(0, 0, 20);
	
		//thrusters
		triangle(-game.drone.hbw/2-4, 7, -game.drone.hbw/2, 12, -game.drone.hbw/2+4, 7)
		rect(-game.drone.hbw/2, 0, 8, 14)
		quad(-game.drone.hbw/2-4, -7, -game.drone.hbw/2-8, -12, -game.drone.hbw/2+8, -12, -game.drone.hbw/2+4, -7)
		triangle(game.drone.hbw/2-4, 7, game.drone.hbw/2, 12, game.drone.hbw/2+4, 7)
		rect(game.drone.hbw/2, 0, 8, 14)
		quad(game.drone.hbw/2-4, -7, game.drone.hbw/2-8, -12, game.drone.hbw/2+8, -12, game.drone.hbw/2+4, -7)
		
		// damage indication
		if(random(8, 20) < sqrt(game.drone.damage)){
			let qq = random(0, 1)
			let xx = qq*game.drone.xr + (1 - qq)*game.drone.xl
			let yy = qq*game.drone.yr + (1 - qq)*game.drone.yl
			let vyy = game.drone.vy + random(3, 12)
			let vxx = game.drone.vx + random(-5, 5)
			particle_system.add(xx, yy, vxx, vyy, 4 + random(0, 2), game.drone.broken ? '#cc3b02' : '#e4c6fa')
		}
		pop()

		// update HUD values
		fuel_meter.value = game.drone.fuel
		health_meter.value = 100 - game.drone.damage
	}
}

function keyPressed() {
	// Enter
  	if(keyCode == 13){
		if(show_title){
			startGame()
		}else if(game.cleared || replay_mode){
			if(daily_mode){
				backToMenu()
			}else{
				if(saved_replay){
					resetRoom()
					saved_replay = undefined
				}else{
					nextLevel(curr_level)
				}
				clearReplay()
			}
		}
	}
	// Backspace
	else if(keyCode == 8 && !show_title){
		resetRoom()
		clearReplay()
		return false
	}
	// Space
	else if(keyCode == 32 && game.cleared){
		startReplay()
	}
	// Escape
	else if(keyCode == 27){
		backToMenu()
	}
	// // F
	// else if(keyCode == 70){
	// 	fullscreen(!fullscreen())
	// 	resizeCanvas(windowWidth, windowHeight)
	// }
	// T
	else if(show_title && keyCode == 84){
		startGame(1)
		clearReplay()
	}
	// D
	else if(show_title && keyCode == 68){
		daily_mode = true
		let daily_level = getDailySeed()
		startGame(daily_level)
	}
	// Left thrust - Q, Right arrow
	else if(keyCode == 81 || keyCode == 39){
		thrust_left = true
	}
	// Right thrust - P, Left arrow, E
	else if(keyCode == 80 || keyCode == 37 || keyCode == 69){
		thrust_right = true
	}
}

function keyReleased(){
	// Left thrust
	// Q, Right arrow 
	if(keyCode == 81 || keyCode == 39){
		thrust_left = false
	}
	// Right thrust
	// P, Left arrow, E
	else if(keyCode == 80 || keyCode == 37 || keyCode == 69){
		thrust_right = false
	}
}

/* prevents the mobile browser from processing some default
 * touch events, like swiping left for "back" or scrolling
 * the page.
 */
function touchStarted(){
	if(is_mobile && touches.length > 0){
		if(show_title){
			if(!info_splash.classList.contains('hidden')){
				return true
			}

			// probably do nothing later when these have been moved to buttons..?
			if(mouseY > height/2 && mouseY < height*3/4){
				if(mouseX < width/3){
					// tutorial
					startGame(1)
					clearReplay()
				}else if(mouseX < width*2/3){
					// start
					startGame()
				}else{
					// daily
					daily_mode = true
					let daily_level = getDailySeed()
					startGame(daily_level)
				}
			}
		}else if(replay_mode){
			if(mouseY < height/4){
				if(mouseX >= width*2/3){
					backToMenu()
				}
			}else{
				if(mouseX < width/3){
					// copy replay link
					// TODO deal with copying on ios bullshit
				}else if(mouseX >= width*2/3){
					// touch right to continue
					if(saved_replay){
						resetRoom()
						saved_replay = undefined
					}else{
						nextLevel(curr_level)
					}
					clearReplay()
				}
			}
		}else if(game.cleared){
			if(mouseY < height/4){
				if(mouseX >= width*2/3){
					backToMenu()
				}
			}else{
				if(mouseX < width/3){
					// replay
					startReplay()
				}else if(mouseX < width*2/3){
					// reset
					resetRoom()
					clearReplay()
				}else{
					// continue
					if(daily_mode){
						backToMenu()
					}else{
						nextLevel(curr_level)
						clearReplay()
					}
				}
			}
		}else{
			if(mouseY < height/4){
				if(mouseX >= width*2/3){
					backToMenu()
				}
			}else{
				thrust_left = false
				thrust_right = false
				touches.forEach(touch => {
					thrust_left |= (touch.x < width/3)
					thrust_right |= (touch.x > width*2/3)
				})
				if(mouseX >= width/3 && mouseX < width*2/3){
					resetRoom()
					clearReplay()
				}
			}
		}
	}
	return false
}

function touchMoved(){
	return false
}

function touchEnded(){
	if(show_title){
		if(!info_splash.classList.contains('hidden')){
			return true
		}
	}

	thrust_left = false
	thrust_right = false
	touches.forEach(touch => {
		thrust_left |= (touch.x < width/3)
		thrust_right |= (touch.x > width*2/3)
	})
	return false
}