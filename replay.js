// Rocket Drone
// Copyright (C) 2022 wavy (wavy@optime.ca)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Replay {

	// take a base64 encoded string and reconstruct the encoded replay data
	constructor(b64){
		if(b64){
			this.moves = []

			// base64 decode
			var binary_string = atob(b64)

			// turn the string into a byte array
			var bytes = new Uint8Array( binary_string.length )
			for (var i = 0; i < binary_string.length; i++){
				bytes[i] = binary_string.charCodeAt(i)
			}

			// reconstruct the replay data
			let view = new Uint16Array(bytes.buffer)

			// start time is encoded with 32 bits
			this.start_time = (view[0] << 16 | view[1])

			let T = 0
			for(let i = 2; i < view.length - 2; i++){
				let x = view[i]
				// last 14 bits of the 16 bit uint encodes the delta ticks
				let t = T + (x & 0x3FFF)
				// first bit is the left command
				let l = (x & 0x8000) != 0 ? 1 : 0
				// second bit is the right command
				let r = (x & 0x4000) != 0 ? 1 : 0
				this.moves.push({t: t, l: l, r: r})
				T = t
			}

			// end time is encoded with 32 bits
			this.end_time = (view[view.length - 2] << 16 | view[view.length - 1])

			this.i_prev = 0
		}else{
			this.moves = []
			this.i_prev = 0
			this.end_time = 0
			this.start_time = 0
		}
	}
	
	// find the move to apply at tick t
	at(t){
		// if the tick at index i_prev is behind tick t, then we can start looking from i_prev instead of 0
		let look_behind = true
		let move = {t: t, l: 0, r: 0}
		for(let i = this.i_prev; i < this.moves.length; i++){
			if(t >= this.moves[i].t){
				move = this.moves[i]
				look_behind = false
			}else{
				break
			}
			this.i_prev = i
		}
		
		// not found after i_prev, so must be before
		if(look_behind){
			for(let i = this.i_prev; i >= 0; i--){
				this.i_prev = i
				if(t >= this.moves[i].t){
					move = this.moves[i]
					break
				}
			}
		}
		
		return move
	}
	
	// remove the replay
	clear(){
		this.moves.length = 0
		this.i_prev = 0
		this.end_time = 0
	}
	
	// add a move
	add(t, l, r){
		if(this.moves.length == 0 || this.moves[this.moves.length-1].l != l || this.moves[this.moves.length-1].r != r){
			this.moves.push({t: t, l: l, r: r})
		}
	}

	// complete the replay by supplying its start and end time
	end(start, end){
		this.start_time = start
		this.end_time = end
	}

	// make some cereal with ice
	cereal(){
		// 2 bytes per move plus 2 uint32 for start and end time
		let n_bytes = 2*this.moves.length + 2*4
		let buffer = new ArrayBuffer(n_bytes)
		let view = new Uint16Array(buffer)
		view[0] = (this.start_time & 0xFFFF0000) >> 16
		view[1] = (this.start_time & 0x0000FFFF)
		let T = 0
		let i = 2
		this.moves.forEach(move => {
			let x = (move.t - T) & 0x3FFF
			if(move.l > 0){
				x |= 0x8000
			}
			if(move.r > 0){
				x |= 0x4000
			}
			view[i++] = x
			T = move.t
		})
		view[view.length - 2] = (this.end_time & 0xFFFF0000) >> 16
		view[view.length - 1] = (this.end_time & 0x0000FFFF)

		let str = ''
		let bytes = new Uint8Array( buffer )
		for (let j = 0; j < bytes.byteLength; j++) {
			str += String.fromCharCode( bytes[ j ] );
		}
		return btoa(str)
	}
}