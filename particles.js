// Rocket Drone
// Copyright (C) 2022 wavy (wavy@optime.ca)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Particle {
	constructor(x, y, vx, vy, T, c){
		this.x = x
		this.y = y
		this.vx = vx
		this.vy = vy
		this.T = T
		this.c = c
	}
	
	update(dt){
		this.T -= dt
		if(this.T <= 0){
			return false
		}
		
		this.x += this.vx*dt
		this.y += this.vy*dt
		
		return true
	}
}

class ParticleSystem {
	constructor(){
		this.particles = []
		this.dt = 0.2
		this.limit = 150
	}
	
	update(){
		for(let i = this.particles.length - 1; i >= 0; i--){
			if(!this.particles[i].update(this.dt)){
				// expired, remove
				this.particles.splice(i, 1)
			}
		}
	}
	
	add(x, y, vx, vy, T, c){
		if(this.particles.length < this.limit){
			this.particles.push(new Particle(x, y, vx, vy, T, c))
		}
	}

	clear(){
		this.particles.length = 0
	}
}